from django.urls import path
from . import views

urlpatterns = [
    path('tickers', views.Tickers.as_view(), name='tickers'),
    path('tickers/prices', views.TickerTrends.as_view(), name='ticker-prices')
]
