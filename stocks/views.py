import requests
from django.conf import settings
from rest_framework.response import Response
from rest_framework.request import Request

from rest_framework.views import APIView

from helpers.cache import cached

provider = settings.QUANDLE

required_columns = ['Date', 'Open', 'High', 'Low', 'Close', 'Adj. Close', 'Volume']


def _map_names_(columns: list, row: list) -> dict:
    ret = {}

    for key, value in enumerate(columns):
        if value in required_columns:
            value = str(value).replace('.', '')
            ret[value] = row[key]

    return ret


class Tickers(APIView):

    @cached
    def get(self, request: Request) -> Response:
        with open(settings.BASE_DIR / 'companies.csv') as f:
            return Response({
                'tickers': [line.replace('\n', '') for line in f.readlines()]
            })


class TickerTrends(APIView):

    @cached
    def get(self, request: Request) -> Response:
        params = request.query_params

        r = requests.get('{}/WIKI/{}.json'.format(settings.QUANDLE['api_root'], params['ticker']),
                         params={'start_date': params['start_date'], 'end_date': params['end_date'],
                                 'api_key': settings.QUANDLE['api_key']})
        json = r.json()
        print(json)
        trends: list = []

        if 'dataset' in json:
            trends = [_map_names_(json['dataset']['column_names'], row) for row in json['dataset']['data'][::-1]]
        return Response({'trends': trends})
