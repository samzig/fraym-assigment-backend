import json

from django.conf import settings

from rest_framework.response import Response
from rest_framework.request import Request

import redis as __redis

redis = __redis.Redis(host=settings.REDIS['host'], port=settings.REDIS['port'], password=settings.REDIS['password'])


def __cache(key: str, value: str):
    redis.set(key, value, ex=12000)


def cached(function):
    def wrapper(*args, **kwargs):
        if len(args) > 1:
            request: Request = args[1]
            key: str = request.get_full_path()
            data: str = redis.get(key)
            if data:
                return Response(json.loads(data))
            response: Response = function(args[0], args[1])
            __cache(key, json.dumps(response.data))
            return response
        return function(args, kwargs)

    return wrapper
