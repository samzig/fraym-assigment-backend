from rest_framework.reverse import reverse
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET'])
def api_root(request, format=None):
    """
    Defines the API root

    :param request:
    :param format:
    :return Response:
    """
    urls = {
        'Stocks': {
            'Tickers': reverse('tickers', request=request, format=format),
            'Tickers Prices': reverse('ticker-prices', request=request, format=format),
        }
    }
    return Response(urls)
